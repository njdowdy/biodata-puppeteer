const puppeteer = require("puppeteer");

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(
    "https://www.nic.funet.fi/pub/sci/bio/life/insecta/lepidoptera/ditrysia/noctuoidea/arctiidae/arctiinae/opharus/"
  );
  await page.screenshot({ path: "example.png" });

  await browser.close();
})();
