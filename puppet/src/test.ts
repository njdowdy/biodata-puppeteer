import * as puppeteer from "puppeteer";

const scrapeInfoFromPage = async (page: puppeteer.Page): Promise<string[]> => {
  const ulListItems = await page.$$("ul.SP > ul");

  // Use Promise.all to scrape the text from each ul list item concurrently
  const ulTextPromises = ulListItems.map(async (ul) => {
    const span = await ul.$("span.TN");
    return span?.evaluate((node) => node.textContent);
  });

  // Wait for all ul list item text to be scraped
  const ulText = await Promise.all(ulTextPromises);

  return ulText;
};

const main = async () => {
  const browser = await puppeteer.launch();
  const pages = await browser.pages();

  // await scrapeInfoFromPages(pages);
  console.log();

  await browser.close();
};

main();
