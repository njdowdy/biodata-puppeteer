import * as puppeteer from "puppeteer";
import { Client } from "pg";

const urls = [
  "https://www.nic.funet.fi/pub/sci/bio/life/insecta/lepidoptera/ditrysia/noctuoidea/arctiidae/arctiinae/opharus/",
  "https://www.nic.funet.fi/pub/sci/bio/life/insecta/lepidoptera/ditrysia/noctuoidea/arctiidae/arctiinae/pithea/",
];

const client = new Client({
  host: "172.29.1.1",
  port: 5432,
  user: "postgres",
  password: "postgres",
  database: "postgres",
});

async function scrapeUrls(browser: puppeteer.Browser, urls: string[]) {
  const pagePromises = urls.map(async (url) => {
    const page: puppeteer.Page = await browser.newPage();
    try {
      // go to the page and wait for the elements to load
      await page.goto(url, { waitUntil: "domcontentloaded" });
      await page.waitForSelector("ul.SP");

      // extract the text from the elements
      const listItems = await page.$$("ul.SP li:not(.SPP ul)");
      //   Use Promise.all to scrape all URLs concurrently
      const results = await Promise.all(
        listItems.map(async (li) => {
          const span = await li.$("span.TN");
          return span
            ? await page.evaluate((span) => span.textContent, span)
            : "";
        })
      );

      return results.filter((e) => e);
    } finally {
      // close the page
      await page.close();
    }
  });

  // wait for all pages to be scraped
  const pageResults = await Promise.all(pagePromises);

  // flatten the results into a single array
  const results = pageResults.flat();

  return results;
}

async function insertIntoDb(results: string[]) {
  try {
    // connect to the database
    await client.connect();

    // insert the data into the database
    for (const text of results) {
      await client.query("INSERT INTO scraped (text) VALUES ($1)", [text]);
      // await client.query('INSERT INTO scraped (id, text) VALUES (DEFAULT, $1)', [text]);
    }
  } finally {
    // close the database connection
    await client.end();
  }
}

async function run() {
  // create a new browser instance
  const browser = await puppeteer.launch();

  try {
    // scrape the urls
    const results = await scrapeUrls(browser, urls);
    console.log(results);
    // insert the data into the database
    //await insertIntoDb(results);
  } finally {
    await browser.close();
  }
}

run();
