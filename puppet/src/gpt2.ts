import * as puppeteer from "puppeteer";
import { Client } from "pg";

const urls = ["http://example.com/page1", "http://example.com/page2"];

const client = new Client({
  host: "172.29.1.1",
  port: 5432,
  user: "postgres",
  password: "postgres",
  database: "postgres",
});

const scrapeInfoFromPage = async (page: puppeteer.Page): Promise<string[]> => {
  const ulListItems = await page.$$("ul.SP > ul");

  // Use Promise.all to scrape the text from each ul list item concurrently
  const ulTextPromises = ulListItems.map(async (ul) => {
    const span = await ul.$("span.TN");
    return span.evaluate((node) => node.textContent);
  });

  // Wait for all ul list item text to be scraped
  const ulText = await Promise.all(ulTextPromises);

  return ulText;
};

const scrapeInfoFromPages = async (pages: puppeteer.Page[], urls: string[]) => {
  // Use Promise.all to navigate to each URL concurrently
  const navigationPromises = urls.map(async (url) => {
    return page.goto(url);
  });

  // Wait for all pages to navigate to the URLs
  await Promise.all(navigationPromises);

  // Use Promise.all to scrape the info from each page concurrently
  const pageInfoPromises = pages.map(async (page) => {
    return scrapeInfoFromPage(page);
  });

  // Wait for all pages to be scraped
  const pageInfo = await Promise.all(pageInfoPromises);

  // Flatten the page info into a single array
  const info = pageInfo.flat();

  
};

async function insertIntoDb(texts: string[]) {
  // Connect to the postgres database and insert the scraped data
  try {
    await client.connect();
    for (const text of info) {
      await client.query("INSERT INTO scraped (text) VALUES ($1)", [text]);
      // await postgresDb.query('INSERT INTO scraped (id, text) VALUES (DEFAULT, $1)', [text]); // ?
    }
  } finally {
    await client.end();
  }
}

const main = async () => {
  // create a new browser instance
  const browser = await puppeteer.launch();
  const pages = await browser.pages();

  await scrapeInfoFromPages(pages, urls);

  await browser.close();
};

main();
