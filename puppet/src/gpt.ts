import puppeteer from "puppeteer";
// const puppeteer = require("puppeteer");

const scrapeInfoFromPage = async (page: puppeteer.Page): Promise<string[]> => {
  const ulListItems = await page.$$("ul.SP > ul");

  // Use Promise.all to scrape the text from each ul list item concurrently
  const ulTextPromises = ulListItems.map(async (ul) => {
    const span = await ul.$("span.TN");
    return span.evaluate((node) => node.textContent);
  });

  // Wait for all ul list item text to be scraped
  const ulText = await Promise.all(ulTextPromises);

  return ulText;
};


const scrapeInfoFromPages = async (pages: puppeteer.Page[], urls: string[]) => {
  // Use Promise.all to navigate to each URL concurrently
  const navigationPromises = urls.map(async (url) => {
    return page.goto(url);
  });

  // Wait for all pages to navigate to the URLs
  await Promise.all(navigationPromises);

  // Use Promise.all to scrape the info from each page concurrently
  const pageInfoPromises = pages.map(async (page) => {
    return scrapeInfoFromPage(page);
  });

  // Wait for all pages to be scraped
  const pageInfo = await Promise.all(pageInfoPromises);

  // Flatten the page info into a single array
  const info = pageInfo.flat();

  // Store the info in the postgres database
  // TODO: implement database connection and query
  const client = new Client();
  await client.connect();
  await client.query("INSERT INTO table_name (info) VALUES ($1)", [info]);
  await client.end();
};


const scrapeInfoFromPages = async (pages: puppeteer.Page[], urls: string[]) => {
  // Use Promise.all to scrape the info from each page concurrently
  const pageInfoPromises = pages.map(async (page) => {
    return scrapeInfoFromPage(page);
  });

  // Wait for all pages to be scraped
  const pageInfo = await Promise.all(pageInfoPromises);

  // Flatten the page info into a single array
  const info = pageInfo.flat();

  // Store the info in the postgres database
  // TODO: implement database connection and query
  const client = new Client();
  await client.connect();
  await client.query("INSERT INTO table_name (info) VALUES ($1)", [info]);
  await client.end();
};

const main = async () => {
  const browser = await puppeteer.launch();
  const pages = await browser.pages();

  const urls = ['http://example.com/page1', 'http://example.com/page2', ...];

  await scrapeInfoFromPages(pages, urls);

  await browser.close();
};


main();

// This script uses Puppeteer to launch a browser, open multiple pages, and scrape the text from the <span class="TN"> elements within each <ul> list item within a <ul class="SP"> element on each page. It uses Promises and Promise.all to scrape the information concurrently from each page and each list item. It then stores the scraped information in a postgres database using a database client.

// Note: You will need to install the puppeteer and postgres npm packages and replace the TODO comment with the appropriate code to connect to your postgres database and insert the scraped information into a table.
